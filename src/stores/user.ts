import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "./types/User";

export const useUserStore = defineStore("user", () => {
  const dialog = ref(false);
  const isTable = ref(true);
  const edited = ref<User>({ id: -1, login: "", name: "", password: "" });
  let lastId = 4;
  const users = ref<User[]>([
    { id: 1, login: "admin", name: "Admin", password: "Pass@1234" },
    { id: 2, login: "user1", name: "User 1", password: "Pass@1234" },
    { id: 3, login: "user2", name: "User 2", password: "Pass@1234" },
  ]);
  const deleteUser = (id: number): void => {
    const index = users.value.findIndex((item) => {
      return item.id === id;
    });
    users.value.splice(index, 1);
  };

  const login = (loginName: string, password: string): boolean => {
    const index = users.value.findIndex((item) => item.login == loginName);
    if (index >= 0) {
      const user = users.value[index];
      if (user.password == password) {
        return true;
      }
      return false;
    }
    return false;
  };

  const saveUser = () => {
    if (edited.value.id < 0) {
      edited.value.id = lastId++;
      users.value.push(edited.value);
    } else {
      const index = users.value.findIndex((item) => {
        return item.id === edited.value.id;
      });
      users.value[index] = edited.value;
    }
    dialog.value = false;
    clear();
  };

  const editUser = (user: User) => {
    edited.value = { ...user };
    dialog.value = true;
  };

  const clear = () => {
    edited.value = { id: -1, login: "", name: "", password: "" };
  };
  return {
    users,
    deleteUser,
    dialog,
    edited,
    clear,
    saveUser,
    editUser,
    isTable,
    login,
  };
});
